import _ from 'lodash'

// -------------------------
// image loading logic

function buildImage (dataUrl) {
  const img = new Image()
  img.src = dataUrl
  return img
}

function getImageData (img) {
  const canvas = document.createElement('canvas')
  canvas.width = img.width
  canvas.height = img.height
  const ctx = canvas.getContext('2d')
  ctx.drawImage(img, 0, 0)
  return ctx.getImageData(0, 0, img.width, img.height)
}

// -------------------------
// aux

function easeIn (t) {
  return Math.pow(t, 5)
}

const floor = Math.floor

// -------------------------
// raster logic

function getImageAverage (data, width, height) {
  const len = height * width
  let avg = 0
  for (let i = 0; i < len; i++) {
    const p = i * 4;
    const r = data[p + 0]
    const g = data[p + 1]
    const b = data[p + 2]
    const a = data[p + 3] - 128
    avg += ((r + g + b) / 3) + a
  }
  return (avg / len) / 255
}

function getRectAvg (data, w, h, x1, y1, x2, y2) {
  let avg = 0
  let len = 0
  for (let y = y1; y < y2; y++) {
    const offset = y * w * 4
    for (let x = x1; x < x2; x++) {
      const r = data[offset + x * 4 + 0]
      const g = data[offset + x * 4 + 1]
      const b = data[offset + x * 4 + 2]
      const a = data[offset + x * 4 + 3] - 128
      len++
      avg += ((r + b + g ) / 3) + a
    }
  }
  return (avg / len) / 255
}

function rasterPixels (data, swidth, sheight, twidth, theight, negative) {
  const average = getImageAverage(data, swidth, sheight)
  const xratio = swidth / twidth
  const yratio = sheight / theight
  const pixels = []
  const op = negative ? _.lte : _.gte
  for (let y = 0; y < theight; y++) {
    for (let x = 0; x < twidth; x++) {
      const rectAvg = getRectAvg(data, swidth, sheight,
                                 floor(x * xratio), floor(y * yratio),
                                 (x + 1) * xratio, (y + 1) * yratio)
      if (op(rectAvg, average)) {
        pixels.push({ x, y, avg: rectAvg})
      }
    }
  }
  return pixels
}

// -------------------------
// svg building

function pixelToSvgCircle ({ x, y }, radius, separation) {
  const offsetx = (radius * 2 + separation) * x
  const offsety = (radius * 2 + separation) * y
  const svgX = offsetx + radius
  const svgY = offsety + radius
  const svgStyle = 'fill: rgb(255, 255, 255);'
  return `<circle cx="${svgX}" cy="${svgY}" r="${radius}" style="${svgStyle}" ></circle>`
}

function pixelsToSvg (pixels, w, h, radius, separation) {
  const circles =  _.join(_.map(pixels,
                                px => pixelToSvgCircle(px, radius, separation)),
                          '\n')
  const vw = (radius * 2 + separation) * w
  const vh = (radius * 2 + separation) * h
  const ns = 'http://www.w3.org/2000/svg'
  const svgText = `<svg xmlns="${ns}" viewBox="${`0 0 ${vw} ${vh}`}">\n${circles}\n</svg>`
  const svg = document.createElementNS(ns, 'svg')
  svg.setAttribute('viewBox', `0 0 ${vw} ${vh}`)
  svg.innerHTML = circles
  return { svg, svgText }
}

// -------------------------
// entry point

function fromImageData (imageData, {width: outputWidth, radius, separation, negative }, cb) {
  // console.log('settings', outputWidth, radius, separation, negative)
  const img = buildImage(imageData)
  img.onload = () => {
    const { data, height, width } = getImageData(img)
    const r = width / height
    const outputHeight = Math.floor(outputWidth / r)
    const pixels = rasterPixels(data, width, height, outputWidth, outputHeight, negative)
    const result = pixelsToSvg(pixels, outputWidth, outputHeight, radius, separation)
    cb(result)
  }

 
}

export default { fromImageData }
