// -------------------------
// dependencies

import _ from 'lodash'
import Promise from 'bluebird'
import pixellify from 'lib/pixellify'
import dat from 'lib/dat.gui'

// -------------------------
// gui

const settings = {
  width: 70,
  radius: 5,
  separation: 5,
  negative: false
};

function showGUI() {
  const gui = new dat.GUI()
  gui.add(settings, 'width', 5, 200).onChange(refresh)
  gui.add(settings, 'radius', 0.5, 40).onChange(refresh)
  gui.add(settings, 'negative').onChange(refresh)
  // gui.add(settings, 'separation', 0.5, 20).onChange(refresh)
}


// -------------------------
// event handlers

let inputData;

function showSvg (svg) {
  // TODO: pending
  const container = document.getElementById('container')
  container.innerHTML = ''
  container.appendChild(svg)
}

function showDownloadLink (data) {
  const a = document.createElement('a')
  const container = document.getElementById('download-button-container')
  container.innerHTML = ''
  a.setAttribute('href', `data:text/plain;charset=utf-8,${encodeURIComponent(data)}`)
  a.setAttribute('download', 'image.svg')
  a.className = 'button button-primary download'
  a.setAttribute('id', 'download-link')
  a.innerHTML = 'Descarga la Imagen'
  container.appendChild(a)
}


function refresh () {
  const { width, radius, separation } = settings
  pixellify.fromImageData(inputData, settings, (svgOutput) => {
    showSvg(svgOutput.svg)
    showDownloadLink(svgOutput.svgText)
  })
}

function onInputFileChange (e) {
  const file = e.target.files[0]
  const reader = new FileReader()
  reader.onload = (e) => {
    inputData = e.target.result
    refresh()
  }
  reader.readAsDataURL(file)
}

function onMainInputFileChange (e) {
  document.getElementById('main-page').style.display = 'none'
  document.getElementById('download-page').style.display = 'block'
  showGUI()
  onInputFileChange(e)
}

// -------------------------
// entry point

window.onload = function() {
  document
    .getElementById('input')
    .addEventListener('change', onInputFileChange, false)

  document
    .getElementById('input-main')
    .addEventListener('change', onMainInputFileChange, false)
}

